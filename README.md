# １回生用ROS入門
## はじめに  
まず、自分のBitbucketのアカウントが作成済みであることを確認してください。  
次に、左の「＋」マークをクリックし、Forkをクリック。  
自分のページにリポジトリをコピーできます。  

LocalにCloneするときは自分のページからCloneしてください  

## このリポジトリについて
このリポジトリは１回生にROSについて知ってもらうために、簡単なコードをまとめたものです。  
ここにあるコードがROSの全てではありませんが、勉強するきっかけになることを願っています。

### joy_twist使用時の注意
/script/の中のjoy_twist.cppはそのままでは使えません。  
まずはコントローラー操作用のパッケージをインストール。  

    $ sudo apt-get install ros-kinetic-joy
    $ sudo apt-get install python-pip
    $ sudo pip install ds4drv


これで準備は完了。コントローラーの動作確認は以下の通り。  

    $ sudo ds4drv
    $ roscore
    $ rosrun joy joy_node
    $ rostopic echo joy

